CREATE
------------------
1.Write a SQL statement to create a simple table countries including columns country_id,country_name and region_id


2.Write a SQL statement to create a table countries set a constraint NULL

mysql> desc countries;
+--------------+-------------+------+-----+---------+----------------+
| Field        | Type        | Null | Key | Default | Extra          |
+--------------+-------------+------+-----+---------+----------------+
| country_id   | int(11)     | NO   | PRI | NULL    | auto_increment |
| country_name | varchar(30) | YES  |     | NULL    |                |
| region_id    | int(11)     | YES  |     | NULL    |                |
+--------------+-------------+------+-----+---------+----------------+
3 rows in set (0.06 sec)


mysql> use bhaskar;
Database changed

3.create table locations including columns.
mysql> create table locations(LOCATION_ID  decimal(4,0),STREET_ADDRESS  varchar(40),POSTAL_CODE varchar(12),CITY   varchar(30) ,STATE_PROVINCE varchar(25) ,COUNTRY_ID  varchar(2)  );
Query OK, 0 rows affected (0.13 sec)

mysql> desc locations;
+----------------+--------------+------+-----+---------+-------+
| Field          | Type         | Null | Key | Default | Extra |
+----------------+--------------+------+-----+---------+-------+
| LOCATION_ID    | decimal(4,0) | YES  |     | NULL    |       |
| STREET_ADDRESS | varchar(40)  | YES  |     | NULL    |       |
| POSTAL_CODE    | varchar(12)  | YES  |     | NULL    |       |
| CITY           | varchar(30)  | YES  |     | NULL    |       |
| STATE_PROVINCE | varchar(25)  | YES  |     | NULL    |       |
| COUNTRY_ID     | varchar(2)   | YES  |     | NULL    |       |
+----------------+--------------+------+-----+---------+-------+
6 rows in set (0.02 sec)



ALTER
-------------


1.Write a SQL statement to rename the table countries to country_new.
mysql> rename table countries to country_new;
Query OK, 0 rows affected (0.11 sec)

mysql> desc country_new;
+--------------+-------------+------+-----+---------+-------+
| Field        | Type        | Null | Key | Default | Extra |
+--------------+-------------+------+-----+---------+-------+
| country_id   | int(11)     | YES  |     | NULL    |       |
| country_name | varchar(30) | YES  |     | NULL    |       |
| region_id    | int(11)     | YES  |     | NULL    |       |
+--------------+-------------+------+-----+---------+-------+
3 rows in set (0.01 sec)
 2.Write a SQL statement to add a columns ID as the first column of the table locations
alter table locations add columns_ID int first;
Query OK, 0 rows affected (0.21 sec)
Records: 0  Duplicates: 0  Warnings: 0
3.Write a SQL statement to add a column region_id after state_province to the table locations

 

mysql> alter table locations add columnregion_id int after state_province ;
Query OK, 0 rows affected (0.28 sec)
Records: 0  Duplicates: 0  Warnings: 


4.Write a SQL statement change the data type of the column country_id to integer in the table locations
 

mysql> ALTER TABLE locations
    -> MODIFY country_id INT;
Query OK, 0 rows affected (0.29 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> desc locations;
+-----------------+--------------+------+-----+---------+-------+
| Field           | Type         | Null | Key | Default | Extra |
+-----------------+--------------+------+-----+---------+-------+
| columns_ID      | int(11)      | YES  |     | NULL    |       |
| LOCATION_ID     | decimal(4,0) | YES  |     | NULL    |       |
| STREET_ADDRESS  | varchar(40)  | YES  |     | NULL    |       |
| POSTAL_CODE     | varchar(12)  | YES  |     | NULL    |       |
| CITY            | varchar(30)  | YES  |     | NULL    |       |
| STATE_PROVINCE  | varchar(25)  | YES  |     | NULL    |       |
| columnregion_id | int(11)      | YES  |     | NULL    |       |
| country_id      | int(11)      | YES  |     | NULL    |       |
+-----------------+--------------+------+-----+---------+-------+
8 rows in set (0.03 sec)


INSERT
-----------

1.Write a SQL statement to insert 3 rows by a single insert statement
2.Write a SQL statement to insert rows into the table countries in which the value of country_id column will be unique and auto incremented
3.Write a SQL statement to insert rows only for country_id and country_name.



1.
mysql> insert into countries values(1,'india',91),(2,'usa',60),(3,'uk',80);
Query OK, 3 rows affected (0.08 sec)
Records: 3  Duplicates: 0  Warnings: 0

mysql> select *from countries;
+------------+--------------+-----------+
| country_id | country_name | region_id |
+------------+--------------+-----------+
|          1 | india        |        91 |
|          2 | usa          |        60 |
|          3 | uk           |        80 |
+------------+--------------+-----------+
3 rows in set (0.00 sec)




2.
mysql> create  table countries(country_id int primary key auto_increment,country_name varchar(30),region_id int);
Query OK, 0 rows affected (0.20 sec)

mysql> desc countries;
+--------------+-------------+------+-----+---------+----------------+
| Field        | Type        | Null | Key | Default | Extra          |
+--------------+-------------+------+-----+---------+----------------+
| country_id   | int(11)     | NO   | PRI | NULL    | auto_increment |
| country_name | varchar(30) | YES  |     | NULL    |                |
| region_id    | int(11)     | YES  |     | NULL    |                |
+--------------+-------------+------+-----+---------+----------------+
3 rows in set (0.03 sec)



3.

mysql> insert into countries(country_name,region_id)  values('india',91);
Query OK, 1 row affected (0.09 sec)

mysql> select *from countries;
+------------+--------------+-----------+
| country_id | country_name | region_id |
+------------+--------------+-----------+
|          1 | india        |        91 |
+------------+--------------+-----------+
1 row in set (0.00 sec)

mysql> insert into countries(country_name,region_id)  values('usa',90),('uk',80);
Query OK, 2 rows affected (0.09 sec)
Records: 2  Duplicates: 0  Warnings: 0

mysql> select *from countries;
+------------+--------------+-----------+
| country_id | country_name | region_id |
+------------+--------------+-----------+
|          1 | india        |        91 |
|          2 | usa          |        90 |
|          3 | uk           |        80 |
+------------+--------------+-----------+
3 rows in set (0.00 sec)





UPDATE
-----------
1.Write a SQL statement to change the email and commission_pct column of employees table with 'not available' and 0.10 for all employees
2.Write a SQL statement to change the email and commission_pct column of employees table with 'not available' and 0.10 for those employees whose department_id is 110.
3.Write a SQL statement to change salary of employee to 8000 whose ID is 105, if the existing salary is less than 5000.
4. Write a SQL statement to increase the minimum and maximum salary of PU_CLERK by 2000 as well as the salary for those employees by 20% and commission percent by .10.

mysql> create table Employees(EMPLOYEE_ID int,FIRST_NAME varchar(30),EMAIL varchar(30),SALARY int,COMMISSION_PCT int ,DEPARTMENT_ID int);
Query OK, 0 rows affected (0.19 sec)



mysql> insert into employees values(101,'neena','NKOCHHAR',17000.00,0.00,90);
Query OK, 1 row affected (0.08 sec)

mysql> insert into employees values(101,'LEX','LDEHAAN',1700.00,0.00,90);
Query OK, 1 row affected (0.05 sec)

mysql> insert into employees values(101,'ALEXANDER','AHUNOLD',9000.00,0.00,90);
Query OK, 1 row affected (0.08 sec)

mysql> insert into employees values(104,'BRUCE','BERNST',6000.00,0.00,60);
Query OK, 1 row affected (0.05 sec)

mysql> insert into employees values(105,'DAVID','DAUSTIN',4800.00,0.00,60);
Query OK, 1 row affected (0.08 sec)

mysql> insert into employees values(106,'VALLI','VPATABAL',4200.00,0.00,60);
Query OK, 1 row affected (0.08 sec)

mysql> insert into employees values(107,'DIANA','DLORENTZ',12008.00,0.00,110);
Query OK, 1 row affected (0.08 sec)

mysql> insert into employees values(205,'SHELLEY','SHIGGINS',8300.00,0.00,110);
Query OK, 1 row affected (0.07 sec)

mysql> insert into employees values(206,'WILIAM','WGIETZ',8300.00,0.00,110);
Query OK, 1 row affected (0.07 sec)

mysql> SELECT *FROM employees;
+-------------+------------+----------+--------+----------------+---------------+
| EMPLOYEE_ID | FIRST_NAME | EMAIL    | SALARY | COMMISSION_PCT | DEPARTMENT_ID |
+-------------+------------+----------+--------+----------------+---------------+
|         100 | Steven     | SKING    |  24000 |              0 |            90 |
|         101 | neena      | NKOCHHAR |  17000 |              0 |            90 |
|         101 | LEX        | LDEHAAN  |   1700 |              0 |            90 |
|         101 | ALEXANDER  | AHUNOLD  |   9000 |              0 |            90 |
|         104 | BRUCE      | BERNST   |   6000 |              0 |            60 |
|         105 | DAVID      | DAUSTIN  |   4800 |              0 |            60 |
|         106 | VALLI      | VPATABAL |   4200 |              0 |            60 |
|         107 | DIANA      | DLORENTZ |  12008 |              0 |           110 |
|         205 | SHELLEY    | SHIGGINS |   8300 |              0 |           110 |
|         206 | WILIAM     | WGIETZ   |   8300 |              0 |           110 |
+-------------+------------+----------+--------+----------------+---------------+
10 rows in set (0.00 sec)

mysql> update employees EMAIL='not available'set COMMISSION_PCT=0.10;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '='not available'set COMMISSION_PCT=0.10' at line 1
mysql> update employees EMAIL='not available',set COMMISSION_PCT=0.10;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '='not available',set COMMISSION_PCT=0.10' at line 1
mysql> UPDATE employees SET email='not available',
    -> commission_pct=0.10;
Query OK, 10 rows affected (0.05 sec)
Rows matched: 10  Changed: 10  Warnings: 0

mysql> update employees set EMAIL='not available', COMMISSION_PCT=0.10;
Query OK, 0 rows affected (0.11 sec)
Rows matched: 10  Changed: 0  Warnings: 0

mysql> select *from employees;
+-------------+------------+---------------+--------+----------------+---------------+
| EMPLOYEE_ID | FIRST_NAME | EMAIL         | SALARY | COMMISSION_PCT | DEPARTMENT_ID |
+-------------+------------+---------------+--------+----------------+---------------+
|         100 | Steven     | not available |  24000 |              0 |            90 |
|         101 | neena      | not available |  17000 |              0 |            90 |
|         101 | LEX        | not available |   1700 |              0 |            90 |
|         101 | ALEXANDER  | not available |   9000 |              0 |            90 |
|         104 | BRUCE      | not available |   6000 |              0 |            60 |
|         105 | DAVID      | not available |   4800 |              0 |            60 |
|         106 | VALLI      | not available |   4200 |              0 |            60 |
|         107 | DIANA      | not available |  12008 |              0 |           110 |
|         205 | SHELLEY    | not available |   8300 |              0 |           110 |
|         206 | WILIAM     | not available |   8300 |              0 |           110 |
+-------------+------------+---------------+--------+----------------+---------------+



1.


mysql> update employees set EMAIL='not available', COMMISSION_PCT=0.10;
Query OK, 10 rows affected (0.07 sec)
Rows matched: 10  Changed: 10  Warnings: 0

mysql> SELECT *FROM employees;
+-------------+------------+---------------+--------+----------------+---------------+
| EMPLOYEE_ID | FIRST_NAME | EMAIL         | SALARY | COMMISSION_PCT | DEPARTMENT_ID |
+-------------+------------+---------------+--------+----------------+---------------+
|         100 | Steven     | not available |  24000 | 0.10           |            90 |
|         101 | neena      | not available |  17000 | 0.10           |            90 |
|         101 | LEX        | not available |   1700 | 0.10           |            90 |
|         101 | ALEXANDER  | not available |   9000 | 0.10           |            90 |
|         104 | BRUCE      | not available |   6000 | 0.10           |            60 |
|         105 | DAVID      | not available |   4800 | 0.10           |            60 |
|         106 | VALLI      | not available |   4200 | 0.10           |            60 |
|         107 | DIANA      | not available |  12008 | 0.10           |           110 |
|         205 | SHELLEY    | not available |   8300 | 0.10           |           110 |
|         206 | WILIAM     | not available |   8300 | 0.10           |           110 |
+-------------+------------+---------------+--------+----------------+---------------+
10 rows in set (0.00 sec)



2.


mysql> UPDATE employees SET email='not available',COMMISSION_PCT=0.10 where  DEPARTMENT_ID=110;
Query OK, 3 rows affected (0.09 sec)
Rows matched: 3  Changed: 3  Warnings: 0

mysql> SELECT *FROM employees;
+-------------+------------+---------------+--------+----------------+---------------+
| EMPLOYEE_ID | FIRST_NAME | EMAIL         | SALARY | COMMISSION_PCT | DEPARTMENT_ID |
+-------------+------------+---------------+--------+----------------+---------------+
|         100 | Steven     | SKING         |  24000 | 0              |            90 |
|         101 | neena      | NKOCHHAR      |  17000 | 0              |            90 |
|         103 | LEX        | LDEHAAN       |   1700 | 0              |            90 |
|         103 | LEX        | LDEHAAN       |   1700 | 0              |            90 |
|         104 | BRUCE      | BERNST        |   6000 | 0              |            60 |
|         105 | DAVID      | DAUSTIN       |   4800 | 0              |            60 |
|         106 | VALLI      | VPATABAL      |   4200 | 0              |            60 |
|         107 | DIANA      | not available |  12008 | 0.10           |           110 |
|         205 | SHELLEY    | not available |   8300 | 0.10           |           110 |
|         206 | WILIAM     | not available |   8300 | 0.10           |           110 |
+-------------+------------+---------------+--------+----------------+---------------+
10 rows in set (0.00 sec)



3.

mysql> UPDATE employees SET SALARY = 8000 WHERE employee_id = 105 AND salary < 5000;
Query OK, 1 row affected (0.07 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> SELECT *FROM employees;
+-------------+------------+---------------+--------+----------------+---------------+
| EMPLOYEE_ID | FIRST_NAME | EMAIL         | SALARY | COMMISSION_PCT | DEPARTMENT_ID |
+-------------+------------+---------------+--------+----------------+---------------+
|         100 | Steven     | SKING         |  24000 | 0              |            90 |
|         101 | neena      | NKOCHHAR      |  17000 | 0              |            90 |
|         103 | LEX        | LDEHAAN       |   1700 | 0              |            90 |
|         103 | LEX        | LDEHAAN       |   1700 | 0              |            90 |
|         104 | BRUCE      | BERNST        |   6000 | 0              |            60 |
|         105 | DAVID      | DAUSTIN       |   8000 | 0              |            60 |
|         106 | VALLI      | VPATABAL      |   4200 | 0              |            60 |
|         107 | DIANA      | not available |  12008 | 0.10           |           110 |
|         205 | SHELLEY    | not available |   8300 | 0.10           |           110 |
|         206 | WILIAM     | not available |   8300 | 0.10           |           110 |
+-------------+------------+---------------+--------+----------------+---------------+
10 rows in set (0.00 sec)




SELECT
-----------


1.Write a SQL statement to display all the information of all salesmen
2.Write a SQL statement to display specific columns like name and commission for all the salesmen
3.Write a SQL statement to display names and city of salesman, who belongs to the city of Paris
4.Write a query to display the columns in a specific order like order date, salesman id, order number and purchase amount from for all the orders
5.Write a query which will retrieve the value of salesman id of all salesmen, getting orders from the customers in orders table without any repeats
6. Write a SQL statement to display all the information for those customers with a grade of 200
7. 1.Write a SQL query to find all the products with a price between Rs.200 and Rs.600



mysql> use bhaskar;
Database changed
mysql> desc salesman;
+-------------+-------------+------+-----+---------+-------+
| Field       | Type        | Null | Key | Default | Extra |
+-------------+-------------+------+-----+---------+-------+
| salesman_id | int(11)     | YES  |     | NULL    |       |
| name        | varchar(30) | YES  |     | NULL    |       |
| city        | varchar(30) | YES  |     | NULL    |       |
| commission  | varchar(30) | YES  |     | NULL    |       |
+-------------+-------------+------+-----+---------+-------+
4 rows in set (0.07 sec)

mysql> insert into salesman values(5001,'James Hoog','New York',0.15),(5002,'Nail Knite','Paris',0.13),(5005,'Pit Alex','London',0.11),(5006,'Mc Lyon','Paris',0.14),(5003,'Lauson', 'Hen', 0.12),(5007,'Paul Adam','Rome',0.13);
Query OK, 6 rows affected (0.07 sec)
Records: 6  Duplicates: 0  Warnings: 0


1.



mysql> select *from salesman;
+-------------+------------+--------------+------------+
| salesman_id | name       | city         | commission |
+-------------+------------+--------------+------------+
|        5001 | James      | HoogNew York | 0.15       |
|        5001 | James Hoog | New York     | 0.15       |
|        5002 | Nail Knite | Paris        | 0.13       |
|        5005 | Pit Alex   | London       | 0.11       |
|        5006 | Mc Lyon    | Paris        | 0.14       |
|        5003 | Lauson     | Hen          | 0.12       |
|        5007 | Paul Adam  | Rome         | 0.13       |
+-------------+------------+--------------+------------+
7 rows in set (0.00 sec)

2.


mysql> select name,commission from salesman;
+------------+------------+
| name       | commission |
+------------+------------+
| James      | 0.15       |
| James Hoog | 0.15       |
| Nail Knite | 0.13       |
| Pit Alex   | 0.11       |
| Mc Lyon    | 0.14       |
| Lauson     | 0.12       |
| Paul Adam  | 0.13       |
+------------+------------+
7 rows in set (0.00 sec)

3.


mysql> select name,commission from salesman where city='paris';
+------------+------------+
| name       | commission |
+------------+------------+
| Nail Knite | 0.13       |
| Mc Lyon    | 0.14       |
+------------+------------+
2 rows in set (0.06 sec)





4.



mysql> create table orders(ord_no int,purch_amt varchar(30),ord_date varchar(30),customer_id int,salesman_id int);
Query OK, 0 rows affected (0.12 sec)

mysql> insert into orders values(70001,150.5,'05-10-2012',3005,5002),(70009,270.65,'10-09-2012',3001,5005),(70002,65.26,'05-10-2012',3002,5001),(70004,110.5,'17-08-2012',3009,5003),(70007,948.5,'10-09-2012',3005,5002),(70005,2400.6,'27-07-2012',3007,5001),(70008,5760,'10-09-2012',3002,5001),(70010,1983.43,'10-10-2012',3004,5006),(70003,2480.4,'10-10-2012',3009,5003),(70012,250.45,'27-06-2012',3008,5002),(70011,75.29,'17-08-2012',3003,5007),(70013,3045.6,'25-04-2012',3002,5001);
Query OK, 12 rows affected (0.04 sec)
Records: 12  Duplicates: 0  Warnings: 0

mysql> select ord_date,salesman_id,ord_no,purch_amt from orders;
+------------+-------------+--------+-----------+
| ord_date   | salesman_id | ord_no | purch_amt |
+------------+-------------+--------+-----------+
| 05-10-2012 |        5002 |  70001 | 150.5     |
| 10-09-2012 |        5005 |  70009 | 270.65    |
| 05-10-2012 |        5001 |  70002 | 65.26     |
| 17-08-2012 |        5003 |  70004 | 110.5     |
| 10-09-2012 |        5002 |  70007 | 948.5     |
| 27-07-2012 |        5001 |  70005 | 2400.6    |
| 10-09-2012 |        5001 |  70008 | 5760      |
| 10-10-2012 |        5006 |  70010 | 1983.43   |
| 10-10-2012 |        5003 |  70003 | 2480.4    |
| 27-06-2012 |        5002 |  70012 | 250.45    |
| 17-08-2012 |        5007 |  70011 | 75.29     |
| 25-04-2012 |        5001 |  70013 | 3045.6    |
+------------+-------------+--------+-----------+
12 rows in set (0.00 sec)



5.

mysql> select distinct salesman_id from orders;
+-------------+
| salesman_id |
+-------------+
|        5002 |
|        5005 |
|        5001 |
|        5003 |
|        5006 |
|        5007 |
+-------------+
6 rows in set (0.02 sec)





mysql> select *from customers;
+-------------+--------------+------------+-------+-------------+
| customer_id | cust_name    | city       | grade | salesman_id |
+-------------+--------------+------------+-------+-------------+
|        3002 | Nick Rimando | New York   |   100 |        5001 |
|        3005 | Graham Zusi  | California |   200 |        5002 |
|        3001 | Brad Guzan   | London     |    80 |        5005 |
|        3004 | Fabian Johns | Paris      |   300 |        5006 |
|        3007 | Brad Davis   | New York   |   200 |        5001 |
|        3009 | Geoff Camero | Berlin     |   100 |        5003 |
|        3008 | Julian Green | London     |   300 |        5002 |
|        3003 | Jozy altidor | Moscow     |   200 |        5007 |
+-------------+--------------+------------+-------+-------------+
8 rows in set (0.00 sec)




6.


mysql> select customer_id,cust_name,city,grade,salesman_id from customers where grade=200;
+-------------+--------------+------------+-------+-------------+
| customer_id | cust_name    | city       | grade | salesman_id |
+-------------+--------------+------------+-------+-------------+
|        3005 | Graham Zusi  | California |   200 |        5002 |
|        3007 | Brad Davis   | New York   |   200 |        5001 |
|        3003 | Jozy altidor | Moscow     |   200 |        5007 |
+-------------+--------------+------------+-------+-------------+
3 rows in set (0.00 sec)




7.
mysql> create table products(PRO_ID INT,PRO_NAME VARCHAR(30),PRO_PRICE INT,PRO_COM INT);
Query OK, 0 rows affected (0.11 sec)

mysql> insert into Products values(101,'mother board',32000,15 );
Query OK, 1 row affected (0.05 sec)

mysql> insert into Products values(102,'key board',450,16 );
Query OK, 1 row affected (0.07 sec)

mysql> insert into Products values(103,'zip drive',250,14 );
Query OK, 1 row affected (0.04 sec)

mysql> insert into Products values(104,'speakers',550,16 );
Query OK, 1 row affected (0.06 sec)

mysql> insert into Products values(105,'monitor',5000,11 );
Query OK, 1 row affected (0.05 sec)

mysql> insert into Products values(106,'DVD drive',900,12 );
Query OK, 1 row affected (0.07 sec)

mysql> insert into Products values(107,'CD drive',800,12 );
Query OK, 1 row affected (0.08 sec)

mysql> insert into Products values(108,'printer',2600,13 );
Query OK, 1 row affected (0.06 sec)

mysql> insert into Products values(109,'refill cartridge',2600,13 );
Query OK, 1 row affected (0.06 sec)

mysql> insert into Products values(110,'mouse',250,12 );
Query OK, 1 row affected (0.09 sec)

mysql> select *from products;
+--------+------------------+-----------+---------+
| PRO_ID | PRO_NAME         | PRO_PRICE | PRO_COM |
+--------+------------------+-----------+---------+
|    101 | mother board     |     32000 |      15 |
|    102 | key board        |       450 |      16 |
|    103 | zip drive        |       250 |      14 |
|    104 | speakers         |       550 |      16 |
|    105 | monitor          |      5000 |      11 |
|    106 | DVD drive        |       900 |      12 |
|    107 | CD drive         |       800 |      12 |
|    108 | printer          |      2600 |      13 |
|    109 | refill cartridge |      2600 |      13 |
|    110 | mouse            |       250 |      12 |
+--------+------------------+-----------+---------+
10 rows in set (0.00 sec)

7.



mysql> select *from products;
+--------+------------------+-----------+---------+
| PRO_ID | PRO_NAME         | PRO_PRICE | PRO_COM |
+--------+------------------+-----------+---------+
|    101 | mother board     |     32000 |      15 |
|    102 | key board        |       450 |      16 |
|    103 | zip drive        |       250 |      14 |
|    104 | speakers         |       550 |      16 |
|    105 | monitor          |      5000 |      11 |
|    106 | DVD drive        |       900 |      12 |
|    107 | CD drive         |       800 |      12 |
|    108 | printer          |      2600 |      13 |
|    109 | refill cartridge |      2600 |      13 |
|    110 | mouse            |       250 |      12 |
+--------+------------------+-----------+---------+
10 rows in set (0.00 sec)

mysql> select  pro_name from products where pro_price between 200 and 600 ;
+-----------+
| pro_name  |
+-----------+
| key board |
| zip drive |
| speakers  |
| mouse     |
+-----------+
4 rows in set (0.03 sec)

mysql> select  pro_name,pro_price from products where pro_price between 200 and 600 ;
+-----------+-----------+
| pro_name  | pro_price |
+-----------+-----------+
| key board |       450 |
| zip drive |       250 |
| speakers  |       550 |
| mouse     |       250 |
+-----------+-----------+
4 rows in set (0.00 sec)




Aggregation Function
--------------------------------
1.Write a SQL statement to find the total purchase amount of all orders.
2.Write a SQL statement to find the average purchase amount of all orders.
3.Write a SQL statement to find the number of salesmen currently listing for all of their customers
4.Write a SQL statement know how many customer have listed their names.
5.Write a SQL statement to get the maximum purchase amount of all the orders
 




mysql> select *from orders ;
+--------+-----------+------------+-------------+-------------+
| ord_no | purch_amt | ord_date   | customer_id | salesman_id |
+--------+-----------+------------+-------------+-------------+
|  70001 | 150.5     | 05-10-2012 |        3005 |        5002 |
|  70009 | 270.65    | 10-09-2012 |        3001 |        5005 |
|  70002 | 65.26     | 05-10-2012 |        3002 |        5001 |
|  70004 | 110.5     | 17-08-2012 |        3009 |        5003 |
|  70007 | 948.5     | 10-09-2012 |        3005 |        5002 |
|  70005 | 2400.6    | 27-07-2012 |        3007 |        5001 |
|  70008 | 5760      | 10-09-2012 |        3002 |        5001 |
|  70010 | 1983.43   | 10-10-2012 |        3004 |        5006 |
|  70003 | 2480.4    | 10-10-2012 |        3009 |        5003 |
|  70012 | 250.45    | 27-06-2012 |        3008 |        5002 |
|  70011 | 75.29     | 17-08-2012 |        3003 |        5007 |
|  70013 | 3045.6    | 25-04-2012 |        3002 |        5001 |
+--------+-----------+------------+-------------+-------------+
12 rows in set (0.00 sec)
1.

mysql> select sum(purch_amt) from orders;
+----------------+
| sum(purch_amt) |
+----------------+
|          17542 |
+----------------+
1 row in set (0.10 sec)


2.




mysql> select avg(purch_amt) from orders;
+----------------+
| avg(purch_amt) |
+----------------+
|      1461.8333 |
+----------------+
1 row in set (0.00 sec)


5.


mysql> select max(purch_amt) from orders;
+----------------+
| max(purch_amt) |
+----------------+
|           5760 |
+----------------+
1 row in set (0.02 sec)



SORTING and FILTERING
-----------------------------------------------


ysql> select *from employees;
+-------------+------------+---------------+--------+----------------+---------------+
| EMPLOYEE_ID | FIRST_NAME | EMAIL         | SALARY | COMMISSION_PCT | DEPARTMENT_ID |
+-------------+------------+---------------+--------+----------------+---------------+
|         100 | Steven     | SKING         |  24000 | 0              |            90 |
|         101 | neena      | NKOCHHAR      |  17000 | 0              |            90 |
|         103 | LEX        | LDEHAAN       |   1700 | 0              |            90 |
|         103 | LEX        | LDEHAAN       |   1700 | 0              |            90 |
|         104 | BRUCE      | BERNST        |   6000 | 0              |            60 |
|         105 | DAVID      | DAUSTIN       |   8000 | 0              |            60 |
|         106 | VALLI      | VPATABAL      |   4200 | 0              |            60 |
|         107 | DIANA      | not available |  12008 | 0.10           |           110 |
|         205 | SHELLEY    | not available |   8300 | 0.10           |           110 |
|         206 | WILIAM     | not available |   8300 | 0.10           |           110 |
+-------------+------------+---------------+--------+----------------+---------------+
10 rows in set (0.00 sec)



1.



mysql> select first_name from employees where salary<6000 ;
+------------+
| first_name |
+------------+
| LEX        |
| LEX        |
| VALLI      |
+------------+
+-------------+--------------+------------+-------+-------------+
| customer_id | cust_name    | city       | grade | salesman_id |
+-------------+--------------+------------+-------+-------------+
|        3002 | Nick Rimando | New York   |   100 |        5001 |
|        3005 | Graham Zusi  | California |   200 |        5002 |
|        3001 | Brad Guzan   | London     |    80 |        5005 |
|        3004 | Fabian Johns | Paris      |   300 |        5006 |
|        3007 | Brad Davis   | New York   |   200 |        5001 |
|        3009 | Geoff Camero | Berlin     |   100 |        5003 |
|        3008 | Julian Green | London     |   300 |        5002 |
|        3003 | Jozy altidor | Moscow     |   200 |        5007 |
+-------------+--------------+------------+-------+-------------+
8 rows in set (0.00 sec)


3.

mysql> select count(distinct salesman_id) from customers;
+-----------------------------+
| count(distinct salesman_id) |
+-----------------------------+
|                           6 |
+-----------------------------+
1 row in set (0.00 sec)

4.



mysql> select count(*) from customers;
+----------+
| count(*) |
+----------+
|        8 |
+----------+
1 row in set (0.00 sec)



RELATIONAL OPERATOR

---------------------------------------
1.Write a query to display all customers with a grade above 100
mysql> select * from customers where grade >100;
+-------------+--------------+------------+-------+-------------+
| customer_id | cust_name    | city       | grade | salesman_id |
+-------------+--------------+------------+-------+-------------+
|        3005 | Graham Zusi  | California |   200 |        5002 |
|        3004 | Fabian Johns | Paris      |   300 |        5006 |
|        3007 | Brad Davis   | New York   |   200 |        5001 |
|        3008 | Julian Green | London     |   300 |        5002 |
|        3003 | Jozy altidor | Moscow     |   200 |        5007 |
+-------------+--------------+------------+-------+-------------+
5 rows in set (0.00 sec)
2.Write a query statement to display all customers in New York who have a grade value above 100

mysql> select * from customers where city='New York'and grade >100;
+-------------+------------+----------+-------+-------------+
| customer_id | cust_name  | city     | grade | salesman_id |
+-------------+------------+----------+-------+-------------+
|        3007 | Brad Davis | New York |   200 |        5001 |
+-------------+------------+----------+-------+-------------+
1 row in set (0.04 sec)

3.Write a SQL statement to display all customers, who are either belongs to the city New York or had a grade above 100
mysql> select * from customers where city='New York'or grade >100;
+-------------+--------------+------------+-------+-------------+
| customer_id | cust_name    | city       | grade | salesman_id |
+-------------+--------------+------------+-------+-------------+
|        3002 | Nick Rimando | New York   |   100 |        5001 |
|        3005 | Graham Zusi  | California |   200 |        5002 |
|        3004 | Fabian Johns | Paris      |   300 |        5006 |
|        3007 | Brad Davis   | New York   |   200 |        5001 |
|        3008 | Julian Green | London     |   300 |        5002 |
|        3003 | Jozy altidor | Moscow     |   200 |        5007 |
+-------------+--------------+------------+-------+-------------+
6 rows in set (0.05 sec)
4.Write a SQL statement to display either those orders which are not issued on date 2012-09-10 and issued by the salesman whose ID is 505 and below or those orders which purchase amount is 1000.00 and below.
mysql> select * from orders where not(ord_date="10-09-2012") and salesman_id<=5005 or purch_amt=1000;
+--------+-----------+------------+-------------+-------------+
| ord_no | purch_amt | ord_date   | customer_id | salesman_id |
+--------+-----------+------------+-------------+-------------+
|  70001 | 150.5     | 05-10-2012 |        3005 |        5002 |
|  70002 | 65.26     | 05-10-2012 |        3002 |        5001 |
|  70004 | 110.5     | 17-08-2012 |        3009 |        5003 |
|  70005 | 2400.6    | 27-07-2012 |        3007 |        5001 |
|  70003 | 2480.4    | 10-10-2012 |        3009 |        5003 |
|  70012 | 250.45    | 27-06-2012 |        3008 |        5002 |
|  70013 | 3045.6    | 25-04-2012 |        3002 |        5001 |
+--------+-----------+------------+-------------+-------------+
7 rows in set (0.00 sec)


SUB QURIES
---------------------

1.Write a query to display all the orders from the orders table issued by the salesman 'Paul Adam'
mysql> select * from orders where salesman_id=(select salesman_id from salesman where city="london");
+--------+-----------+------------+-------------+-------------+
| ord_no | purch_amt | ord_date   | customer_id | salesman_id |
+--------+-----------+------------+-------------+-------------+
|  70009 | 270.65    | 10-09-2012 |        3001 |        5005 |
+--------+-----------+------------+-------------+-------------+
1 row in set (0.00 sec)


2.Write a query to display all the orders for the salesman who belongs to the city London
select * from orders where salesman_id=(select salesman_id from salesman where city="london");
+--------+-----------+------------+-------------+-------------+
| ord_no | purch_amt | ord_date   | customer_id | salesman_id |
+--------+-----------+------------+-------------+-------------+
|  70009 | 270.65    | 10-09-2012 |        3001 |        5005 |
+--------+-----------+------------+-------------+-------------+
1 row in set (0.00 sec)


3. Write a query to find all the orders issued against the salesman who works for customer whose id is 3007
 SELECT *
    -> FROM orders
    -> WHERE salesman_id =
    ->     (SELECT DISTINCT salesman_id
    ->      FROM orders
    ->      WHERE customer_id =3007);
+--------+-----------+------------+-------------+-------------+
| ord_no | purch_amt | ord_date   | customer_id | salesman_id |
+--------+-----------+------------+-------------+-------------+
|  70002 | 65.26     | 05-10-2012 |        3002 |        5001 |
|  70005 | 2400.6    | 27-07-2012 |        3007 |        5001 |
|  70008 | 5760      | 10-09-2012 |        3002 |        5001 |
|  70013 | 3045.6    | 25-04-2012 |        3002 |        5001 |
+--------+-----------+------------+-------------+-------------+
4 rows in set (0.06 sec)
4.Write a query to display the commission of all the salesmen servicing customers in Paris

mysql> select commission from salesman where salesman_id in(select salesman_id from customers where city ='paris');
+------------+
| commission |
+------------+
| 0.14       |
+------------+
1 row in set (0.09 sec)




JOIN
------------------


1.Write a query in SQL to display the first name, last name, department number, and department name for each employee
2.Write a query in SQL to display the first name, last name, department number and department name, for all employees for departments 80 or 40
3.Write a query in SQL to display the first name of all employees including the first name of their manager
4.Write a query in SQL to display all departments including those where does not have any employee
5.Write a query in SQL to display the first name, last name, department number and name, for all employees who have or have not any department





mysql> select E.FIRST_NAME,D.department_name from employees E join departments D on E.department_id=D.department_id;t
+------------+-----------------+
| FIRST_NAME | department_name |
+------------+-----------------+
| BRUCE      | IT              |
| DAVID      | IT              |
| VALLI      | IT              |
| neena      | excutive        |
| LEX        | excutive        |
| ALEXANDER  | excutive        |
| DIANA      | accounting      |
| SHELLEY    | accounting      |
| WILIAM     | accounting      |
+------------+-----------------+
9 rows in set (0.00 sec)


mysql> select E.FIRST_NAME,D.department_name from employees E join departments D on  D.department_id in(80,40);
+------------+-----------------+
| FIRST_NAME | department_name |
+------------+-----------------+
| neena      | human resoures  |
| LEX        | human resoures  |
| ALEXANDER  | human resoures  |
| BRUCE      | human resoures  |
| DAVID      | human resoures  |
| VALLI      | human resoures  |
| DIANA      | human resoures  |
| SHELLEY    | human resoures  |
| WILIAM     | human resoures  |
| neena      | sales           |
| LEX        | sales           |
| ALEXANDER  | sales           |
| BRUCE      | sales           |
| DAVID      | sales           |
| VALLI      | sales           |
| DIANA      | sales           |
| SHELLEY    | sales           |
| WILIAM     | sales           |
+------------+-----------------+
18 rows in set (0.00 sec)

JDBC
-----------------
package com.jdbcass;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Statement1{

	public static void main(String[] args) throws Exception {
		 Connection con=null;
		  Statement st=null;
		  //ResultSet rs = null;
		  
		  try {
			Class.forName("com.mysql.jdbc.Driver");
			 //DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		    con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","root");
		    st = con.createStatement();
		    // st.execute("create table users(username varchar(30),password varchar(20),fullname varchar(30),email varchar(30))");
		   // int noOfRows=st.executeUpdate("update users set username='steve pauls'");
		    //int noOfRows1=st.executeUpdate("update users set password='steve pauls'");
int noOfRow=st.executeUpdate("delete from users where username='steve'");
		    //int noOfRows2=st.executeUpdate("update users set fullname='steve pauls'");

		    //int noOfRows3=st.executeUpdate("update users set email='steve pauls'");

		  //  int noOfRows=st.executeUpdate("insert into users values('steve','secretpass','steve paul','steve.pual@hcl.com')");
		    //System.out.println(noOfRows+"row inserted");
		   // rs = st.executeQuery("select * from users");
            //while (rs.next()) {
              //  System.out.println(rs.getString("username") + " " +rs.getString("fullname")+" "+rs.getString("email")+" "+rs.getString("password"));
              //  System.out.println(rs.getInt(1)+" "+rs.getString(2));
		  //}
              }
		 
		  
		  catch (Exception e)
		  {
			  System.out.println(e);
		}
		  finally
		  {
			  st.close();
			  con.close();
		  }
		   
	}}

