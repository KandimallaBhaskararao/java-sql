package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.model.Pet;
import com.model.User;

public class BuyPetDao implements DaoInterface{
	
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	private static final String DB_URL = "jdbc:mysql://localhost:3306/petShop";
	private static final String USER = "root";
	private static final String PASS = "root";
	
	private Connection getConnection(){
		Connection connection=null;
		try {
			Class.forName(JDBC_DRIVER);
			connection= DriverManager.getConnection(DB_URL,USER,PASS);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection;
	}


	
	public boolean create(User user) {
		
		return false;
	}

	
	public boolean read(User user) {
		
		return false;
	}

	
	public boolean update(User user,int petId) {
		PreparedStatement preparedStatement = null;
		String query="";
		Connection connection=getConnection();
		try {
			query="UPDATE PETS SET PET_OWNERID=(SELECT ID FROM PET_USER WHERE USER_NAME=?) WHERE ID=?";
			preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1, user.getUserName());
			preparedStatement.setInt(2,petId);
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	
	public boolean delete(String userId) {
		
		return false;
	}

	
	public String readLogin(User user) {
		
		return null;
	}

	
	public boolean savePet(Pet pet) {
		
		return false;
	}

	
	public List<Pet> readPet() {
				return null;
	}

	
	public List<Pet> readPet(User user) {
		
		return null;
	}
	

}
