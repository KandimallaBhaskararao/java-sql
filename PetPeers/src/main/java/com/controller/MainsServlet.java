package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.model.Pet;
import com.model.User;
import com.service.LoginValidator;
import com.service.PetValidator;
import com.service.UserValidator;
@WebServlet("/MainsServlet")
public class MainsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		if(request.getParameter("jspPage").equals("userregn.jsp")){
			response.setContentType("text/html");
			String confirmPassword=request.getParameter("confirmPassword");
			User user=new User();
			user.setUserName(request.getParameter("userName"));
			user.setPassword(request.getParameter("password"));
			UserValidator registrationValidate =new UserValidator();
			if(registrationValidate.validate(user, confirmPassword)){
				out.print("<h1> successfully registered, Please ");
				out.print("</h1>");
				out.print("<a href='login.jsp'>login</a>"); 
			}
			else{
				out.print("<font style=\"color:pink\">"+"useralready there");
			}
		}
		else if(request.getParameter("jspPage").equals("login.jsp")){
			response.setContentType("text/html");
			User user=new User();
			user.setUserName(request.getParameter("userName"));
			user.setPassword(request.getParameter("password"));
			LoginValidator loginValidator=new LoginValidator();
			if(loginValidator.validate(user)){
				HttpSession session = request.getSession();
				session.setAttribute("user", user.getUserName());
				// session to expire in 30 minutes
				session.setMaxInactiveInterval(30*60);
		        response.sendRedirect("PetDetailsServlet");
			}
			else{
				out.println("<font color=pink> user name or password not matched with database.</font>");
			}
		}
		else if(request.getParameter("jspPage").equals("logout.jsp")){
			response.setContentType("text/html");
			request.getSession(false).setAttribute("user", null);
			request.getSession().invalidate();
	        response.sendRedirect("login.jsp");  
		}
		else if(request.getParameter("jspPage").equals("pet_form.jsp")){
			response.setContentType("text/html");
			Pet pet=new Pet();
			pet.setPetName(request.getParameter("name"));
			pet.setPetAge(Integer.parseInt(request.getParameter("age")));
			pet.setPetPlace(request.getParameter("place"));
			PetValidator petValidator =new PetValidator();
			if(petValidator.validate(pet)){
				out.println("<script type=\"text/javascript\">");  
				out.println("alert('petsaved Successfully');");  
				out.println("location='pet_form.jsp';"); 
				out.println("</script>");
			}
			else{
				out.println("<script type=\"text/javascript\">");  
				out.println("alert('Invalid Details');");  
				out.println("location='pet_form.jsp';"); 
				out.println("</script>");
			}
		}
		
		
	}

}